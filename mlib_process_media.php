<!doctype html>
<?php
require('mlib_functions.php');
html_head("mlib skeleton");
require('mlib_header.php');
require('mlib_sidebar.php');

# Code for your web page follows.
echo "<h2>Processing Media from List...</h2>";

if ($_FILES['userfile']['error'] > 0)
{
  echo 'Problem: ';
  switch ($_FILES['userfile']['error'])
  {
    case 1:  echo 'File exceeded upload_max_filesize';  break;
    case 2:  echo 'File exceeded max_file_size';  break;
    case 3:  echo 'File only partially uploaded';  break;
    case 4:  echo 'No file uploaded';  break;
  }
  exit;
}

// Does the file have the right MIME type?
if ($_FILES['userfile']['type'] != 'text/plain')
{
  echo 'Problem: file is not plain text';
  exit;
}

// put the file where we'd like it
$upfile = './uploads/'.$_FILES['userfile']['name'];

if (is_uploaded_file($_FILES['userfile']['tmp_name'])) 
{
  if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $upfile))
  {
    echo 'Problem: Could not move file to destination directory';
    exit;
  }
} 
else 
{
  echo 'Problem: Possible file upload attack. Filename: ';
  echo $_FILES['userfile']['name'];
  exit;
}

echo 'File uploaded successfully<br><br>'; 

// display the file contents
$fp = fopen($upfile, 'r');

if (!$fp)
{
  echo "<p>I could not open $upfile right now.";
  exit;
}

# Read data from the file and process it a line at a time.
while (!feof($fp))
{
  $line = fgets($fp);
  $line_array = explode(',', $line);
  $title = trim($line_array[0]);
  $author = trim($line_array[1]);
  $type = trim($line_array[2]);
  $description = trim($line_array[3]);
  


  

  # Validate equipment entry
  $errors = validate_media($title, $author, $type, $description);
  if (empty($errors)) {
    # Display uploaded entries
    echo "title: $title <br/>";
	echo "author: $author <br/>";
    echo "type: $type <br/>";
    echo "description: $description<br/>";

    # Insert entry into database
    try {
      //open the database
      $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $db->exec("INSERT INTO media (title, author, type, status, user_id, description) VALUES ('$title','$author', '$type', 'active', 0, '$description');");
      // close the database connection
      $db = NULL;
    }
    catch(PDOException $e){
      echo 'Exception : '.$e->getMessage();
      echo "<br/>";
      $db = NULL;
    }
  } else {
    echo "Errors found in media entry:<br/>";
    echo "title: $title<br/>";
	echo "author: $author<br/>";
    echo "type: $type<br/>";
    echo "description: $description<br/>";
    foreach($errors as $error) {
      echo $error."<br/>";
    }
  }
  echo "<br/>";
}

# Close the file since we are done.
fclose($fp);
require('mlib_footer.php');
?>
