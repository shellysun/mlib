<!doctype html>
<?php
require('mlib_functions.php');
html_head("mlib skeleton");
require('mlib_header.php');
require('mlib_sidebar.php');

# Code for your web page follows.
?>
<!-- Display a form to capture information -->
<h2>Upload Media List</h2>
<form enctype="multipart/form-data" action="mlib_process_media.php" method=post>
  <input type="hidden" name="MAX_FILE_SIZE" value="1000">
  Upload this file: <input name="userfile" type="file">
  <input type="submit" value="Upload File">
</form>
<?php
require('mlib_footer.php');
?>
