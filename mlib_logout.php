<!doctype html>
<?php
session_start();
require('mlib_functions.php');
html_head("mlib logout");
require('mlib_header.php');

//save valid_user to see if we were logged in to begin with
$old_user = $_SESSION['valid_user'];
unset($_SESSION['valid_user']);
session_destroy();

require('mlib_sidebar.php');
print "<h2>Log out</h2>";

if  (empty($old_user)) {
  print "You were not logged.<br/>";  
} else {
  print "Logged out<br/>";
}

require('mlib_footer.php');
?>
