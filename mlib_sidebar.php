<aside id="sidebar">
  <nav>
    <h2>Actions</h2>
    <ul>
      <li>
        <a href="mlib_status.php">Media Status</a><br/>
      </li>
      <li>
        <a href="mlib_reserve.php">Reserve media</a><br/>
      </li>
      <li>
        <a href="milb_release.php">Release media</a><br/>
      </li>
      <li>
        <a href="mlib_login.php">Login as Administrator</a><br/>
      </li>    
<?php
  if (!empty($_SESSION['valid_user'])) {
?>
      <hr/>
      <li>
        <a href="mlib_media.php">Add media</a><br/>
      </li>
      <li>
        <a href="mlib_users.php">Add Users</a><br/>
      </li>
      <li>
        <a href="mlib_upload.php">Upload media</a><br/>
      </li>
      <li>
        <a href="mlib_administrator.php">Administrator Config</a><br/>
      </li>
      <li>
        <a href="mlib_logout.php">Log Out</a><br/>
      </li>
<?php
  }
?>
    </ul>
  </nav>
</aside>
